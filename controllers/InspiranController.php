<?php

namespace app\controllers;

use app\models\Inspiran;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InspiranController implements the CRUD actions for Inspiran model.
 */
class InspiranController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Inspiran models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Inspiran::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_inspiran' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Inspiran model.
     * @param int $codigo_inspiran Codigo Inspiran
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_inspiran)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_inspiran),
        ]);
    }

    /**
     * Creates a new Inspiran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Inspiran();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo_inspiran' => $model->codigo_inspiran]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Inspiran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo_inspiran Codigo Inspiran
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_inspiran)
    {
        $model = $this->findModel($codigo_inspiran);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_inspiran' => $model->codigo_inspiran]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Inspiran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo_inspiran Codigo Inspiran
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_inspiran)
    {
        $this->findModel($codigo_inspiran)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Inspiran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo_inspiran Codigo Inspiran
     * @return Inspiran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_inspiran)
    {
        if (($model = Inspiran::findOne(['codigo_inspiran' => $codigo_inspiran])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
