<?php

namespace app\controllers;

use app\models\Ejercian;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EjercianController implements the CRUD actions for Ejercian model.
 */
class EjercianController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Ejercian models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Ejercian::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_ejercian' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ejercian model.
     * @param int $codigo_ejercian Codigo Ejercian
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_ejercian)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_ejercian),
        ]);
    }

    /**
     * Creates a new Ejercian model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Ejercian();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo_ejercian' => $model->codigo_ejercian]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ejercian model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo_ejercian Codigo Ejercian
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_ejercian)
    {
        $model = $this->findModel($codigo_ejercian);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_ejercian' => $model->codigo_ejercian]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ejercian model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo_ejercian Codigo Ejercian
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_ejercian)
    {
        $this->findModel($codigo_ejercian)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ejercian model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo_ejercian Codigo Ejercian
     * @return Ejercian the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_ejercian)
    {
        if (($model = Ejercian::findOne(['codigo_ejercian' => $codigo_ejercian])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
