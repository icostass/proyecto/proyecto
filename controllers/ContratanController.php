<?php

namespace app\controllers;

use app\models\Contratan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContratanController implements the CRUD actions for Contratan model.
 */
class ContratanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Contratan models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Contratan::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_contratan' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contratan model.
     * @param int $codigo_contratan Codigo Contratan
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_contratan)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_contratan),
        ]);
    }

    /**
     * Creates a new Contratan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Contratan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo_contratan' => $model->codigo_contratan]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Contratan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo_contratan Codigo Contratan
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_contratan)
    {
        $model = $this->findModel($codigo_contratan);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_contratan' => $model->codigo_contratan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Contratan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo_contratan Codigo Contratan
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_contratan)
    {
        $this->findModel($codigo_contratan)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Contratan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo_contratan Codigo Contratan
     * @return Contratan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_contratan)
    {
        if (($model = Contratan::findOne(['codigo_contratan' => $codigo_contratan])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
