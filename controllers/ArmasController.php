<?php

namespace app\controllers;

use app\models\Armas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ArmasController implements the CRUD actions for Armas model.
 */
class ArmasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Armas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Armas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_armas' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Armas model.
     * @param int $codigo_armas Codigo Armas
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_armas)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_armas),
        ]);
    }

    /**
     * Creates a new Armas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Armas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo_armas' => $model->codigo_armas]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Armas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo_armas Codigo Armas
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_armas)
    {
        $model = $this->findModel($codigo_armas);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_armas' => $model->codigo_armas]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Armas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo_armas Codigo Armas
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_armas)
    {
        $this->findModel($codigo_armas)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Armas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo_armas Codigo Armas
     * @return Armas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_armas)
    {
        if (($model = Armas::findOne(['codigo_armas' => $codigo_armas])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
