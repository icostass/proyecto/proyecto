<?php

use app\models\Ejercian;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Ejercían';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ejercian-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ejercian', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'codigo_ejercian',
            'nombre_piratas',
            'nombre_profesiones',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Ejercian $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_ejercian' => $model->codigo_ejercian]);
                 }
            ],
        ],
    ]); ?>


</div>
