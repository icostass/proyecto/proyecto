<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Ejercian $model */

$this->title = 'Create Ejercian';
$this->params['breadcrumbs'][] = ['label' => 'Ejercians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ejercian-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
