<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Ejercian $model */

$this->title = 'Update Ejercian: ' . $model->codigo_ejercian;
$this->params['breadcrumbs'][] = ['label' => 'Ejercians', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_ejercian, 'url' => ['view', 'codigo_ejercian' => $model->codigo_ejercian]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ejercian-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
