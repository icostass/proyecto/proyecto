<?php
use yii\helpers\Html;
/** @var yii\web\View $this */

$this->title = 'Mapamundi';
?>

<div>
    <h1 class="fuentegv"><strong>¡Bienvenido/a a bordo polizón/a!</strong></h1>
    <div class="fuentep">
        <p>En esta web podrás obtener información de los piratas más temidos de los siete mares.</p>
        <p>Para ver la información de los corsarios del mar, elige uno de los mares disponibles en el mapa de abajo, una vez elegido</p>
        <p>se podrá indagar sobre las historias de estos piratas e información la mar de interesante.</p>     
   </div>
</div> 
<div class="col-md-auto" style="z-index: +1;">      
            <?= Html::img('@web/img/Mapamundi.png', ['alt' => 'Mapamundi', 'style' => 'width: 100%;']) ?>
</div>

