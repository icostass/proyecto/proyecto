<?php

use app\models\Contratan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Contratan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contratan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Contratan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'codigo_contratan',
            'fecha_contrato',
            'nombre_paises',
            'nombre_piratas',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Contratan $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_contratan' => $model->codigo_contratan]);
                 }
            ],
        ],
    ]); ?>


</div>
