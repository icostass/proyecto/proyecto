<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Contratan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="contratan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fecha_contrato')->textInput() ?>

    <?= $form->field($model, 'nombre_paises')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_piratas')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
