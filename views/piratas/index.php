<?php

use app\models\Piratas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Piratas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="piratas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Piratas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'nombre',
            'mote',
            'f_nacimiento',
            'f_muerte',
            'biografia:ntext',
            'nombre_paises',
            'nombre_barcos',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Piratas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'nombre' => $model->nombre]);
                 }
            ],
        ],
    ]); ?>


</div>
