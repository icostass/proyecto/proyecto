<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Piratas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="piratas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mote')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'f_nacimiento')->textInput() ?>

    <?= $form->field($model, 'f_muerte')->textInput() ?>

    <?= $form->field($model, 'biografia')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'nombre_paises')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_barcos')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
