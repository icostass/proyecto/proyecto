<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Profesiones $model */

$this->title = 'Create Profesiones';
$this->params['breadcrumbs'][] = ['label' => 'Profesiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesiones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
