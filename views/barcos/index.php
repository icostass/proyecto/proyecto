<?php

use app\models\Barcos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Barcos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="barcos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Barcos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'nombre',
            'tipo',
            'numero_canones',
            'capitan',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Barcos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'nombre' => $model->nombre]);
                 }
            ],
        ],
    ]); ?>


</div>
