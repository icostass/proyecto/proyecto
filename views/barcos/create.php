<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Barcos $model */

$this->title = 'Create Barcos';
$this->params['breadcrumbs'][] = ['label' => 'Barcos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="barcos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
