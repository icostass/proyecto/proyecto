<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Inspiran $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="inspiran-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_piratas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_isbn')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
