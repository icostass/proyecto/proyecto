<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Inspiran $model */

$this->title = 'Create Inspiran';
$this->params['breadcrumbs'][] = ['label' => 'Inspirans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inspiran-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
