<?php

use app\models\Inspiran;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Inspiran';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inspiran-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Inspiran', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'codigo_inspiran',
            'nombre_piratas',
            'codigo_isbn',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Inspiran $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_inspiran' => $model->codigo_inspiran]);
                 }
            ],
        ],
    ]); ?>


</div>
