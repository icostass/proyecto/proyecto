<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Inspiran $model */

$this->title = 'Update Inspiran: ' . $model->codigo_inspiran;
$this->params['breadcrumbs'][] = ['label' => 'Inspirans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_inspiran, 'url' => ['view', 'codigo_inspiran' => $model->codigo_inspiran]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="inspiran-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
