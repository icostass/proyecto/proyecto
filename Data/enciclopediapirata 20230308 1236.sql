--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 08/03/2023 12:36:09
-- Server version: 5.5.5-10.1.40-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

--
-- Set default database
--
USE enciclopediapirata;

--
-- Drop table `ejercian`
--
DROP TABLE IF EXISTS ejercian;

--
-- Drop table `profesiones`
--
DROP TABLE IF EXISTS profesiones;

--
-- Drop table `inspiran`
--
DROP TABLE IF EXISTS inspiran;

--
-- Drop table `libros`
--
DROP TABLE IF EXISTS libros;

--
-- Drop table `armas`
--
DROP TABLE IF EXISTS armas;

--
-- Drop table `contratan`
--
DROP TABLE IF EXISTS contratan;

--
-- Drop table `roban`
--
DROP TABLE IF EXISTS roban;

--
-- Drop table `piratas`
--
DROP TABLE IF EXISTS piratas;

--
-- Drop table `barcos`
--
DROP TABLE IF EXISTS barcos;

--
-- Drop table `zonas`
--
DROP TABLE IF EXISTS zonas;

--
-- Drop table `paises`
--
DROP TABLE IF EXISTS paises;

--
-- Set default database
--
USE enciclopediapirata;

--
-- Create table `paises`
--
CREATE TABLE paises (
  nombre varchar(100) NOT NULL,
  gobernante varchar(100) DEFAULT NULL,
  PRIMARY KEY (nombre)
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci,
COMMENT = 'Informaci�n de los paises.';

--
-- Create table `zonas`
--
CREATE TABLE zonas (
  nombre varchar(30) NOT NULL,
  longitud varchar(30) NOT NULL,
  latitud varchar(30) NOT NULL,
  nombre_paises varchar(30) NOT NULL,
  fecha_dominio date DEFAULT NULL,
  PRIMARY KEY (nombre)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 16384,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci,
COMMENT = 'Informaci�n de las zonas.';

--
-- Create foreign key
--
ALTER TABLE zonas
ADD CONSTRAINT fk_zonas_paises FOREIGN KEY (nombre_paises)
REFERENCES paises (nombre);

--
-- Create table `barcos`
--
CREATE TABLE barcos (
  nombre varchar(100) NOT NULL,
  tipo varchar(100) NOT NULL,
  numero_canones int(11) DEFAULT NULL,
  capitan varchar(100) DEFAULT NULL,
  PRIMARY KEY (nombre)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 16384,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci,
COMMENT = 'Informaci�n de los barcos.';

--
-- Create table `piratas`
--
CREATE TABLE piratas (
  nombre varchar(100) NOT NULL,
  mote varchar(100) DEFAULT NULL,
  f_nacimiento date DEFAULT NULL,
  f_muerte date DEFAULT NULL,
  biografia longtext DEFAULT NULL,
  nombre_paises varchar(100) DEFAULT NULL,
  nombre_barcos varchar(100) DEFAULT NULL,
  PRIMARY KEY (nombre)
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci,
COMMENT = 'Informaci�n de los piratas.';

--
-- Create foreign key
--
ALTER TABLE piratas
ADD CONSTRAINT fk_piratas_barcos FOREIGN KEY (nombre_barcos)
REFERENCES barcos (nombre);

--
-- Create foreign key
--
ALTER TABLE piratas
ADD CONSTRAINT fk_piratas_paises FOREIGN KEY (nombre_paises)
REFERENCES paises (nombre);

--
-- Create table `roban`
--
CREATE TABLE roban (
  codigo_roban int(11) NOT NULL AUTO_INCREMENT,
  nombre_piratas varchar(100) DEFAULT NULL,
  nombre_zonas varchar(100) DEFAULT NULL,
  PRIMARY KEY (codigo_roban)
)
ENGINE = INNODB,
AUTO_INCREMENT = 2,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci,
COMMENT = 'Informaci�n de quien robaba en cada zona.';

--
-- Create index `uk_piratas_zonas` on table `roban`
--
ALTER TABLE roban
ADD UNIQUE INDEX uk_piratas_zonas (nombre_piratas, nombre_zonas);

--
-- Create foreign key
--
ALTER TABLE roban
ADD CONSTRAINT fk_roban_piratas FOREIGN KEY (nombre_piratas)
REFERENCES piratas (nombre);

--
-- Create foreign key
--
ALTER TABLE roban
ADD CONSTRAINT fk_roban_zonas FOREIGN KEY (nombre_zonas)
REFERENCES zonas (nombre);

--
-- Create table `contratan`
--
CREATE TABLE contratan (
  codigo_contratan int(11) NOT NULL AUTO_INCREMENT,
  fecha_contrato date DEFAULT NULL,
  nombre_paises varchar(100) NOT NULL,
  nombre_piratas varchar(100) NOT NULL,
  PRIMARY KEY (codigo_contratan)
)
ENGINE = INNODB,
AUTO_INCREMENT = 2,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci,
COMMENT = 'Informaci�n de los contratos.';

--
-- Create index `uk_paises_piratas` on table `contratan`
--
ALTER TABLE contratan
ADD UNIQUE INDEX uk_paises_piratas (nombre_paises, nombre_piratas);

--
-- Create foreign key
--
ALTER TABLE contratan
ADD CONSTRAINT fk_contratan_paises FOREIGN KEY (nombre_paises)
REFERENCES paises (nombre);

--
-- Create foreign key
--
ALTER TABLE contratan
ADD CONSTRAINT fk_contratan_piratas FOREIGN KEY (nombre_piratas)
REFERENCES piratas (nombre);

--
-- Create table `armas`
--
CREATE TABLE armas (
  codigo_armas int(11) NOT NULL AUTO_INCREMENT,
  nombre_piratas varchar(100) NOT NULL,
  armas varchar(50) NOT NULL,
  PRIMARY KEY (codigo_armas)
)
ENGINE = INNODB,
AUTO_INCREMENT = 2,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci,
COMMENT = 'Informaci�n de las armas.';

--
-- Create index `uk_piratas_armas` on table `armas`
--
ALTER TABLE armas
ADD UNIQUE INDEX uk_piratas_armas (nombre_piratas, armas);

--
-- Create foreign key
--
ALTER TABLE armas
ADD CONSTRAINT fk_armas_piratas FOREIGN KEY (nombre_piratas)
REFERENCES piratas (nombre);

--
-- Create table `libros`
--
CREATE TABLE libros (
  isbn varchar(13) NOT NULL,
  nombre varchar(100) NOT NULL,
  fecha_publicacion varchar(4) DEFAULT NULL,
  autor varchar(100) DEFAULT NULL,
  PRIMARY KEY (isbn)
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci,
COMMENT = 'Informaci�n de los libros.';

--
-- Create table `inspiran`
--
CREATE TABLE inspiran (
  codigo_inspiran int(11) NOT NULL AUTO_INCREMENT,
  nombre_piratas varchar(100) DEFAULT NULL,
  codigo_isbn varchar(13) DEFAULT NULL,
  PRIMARY KEY (codigo_inspiran)
)
ENGINE = INNODB,
AUTO_INCREMENT = 2,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci,
COMMENT = 'Informaci�n de qui�n inspir� los libros.';

--
-- Create index `uk_piratas_libros` on table `inspiran`
--
ALTER TABLE inspiran
ADD UNIQUE INDEX uk_piratas_libros (nombre_piratas, codigo_isbn);

--
-- Create foreign key
--
ALTER TABLE inspiran
ADD CONSTRAINT fk_inspiran_libros FOREIGN KEY (codigo_isbn)
REFERENCES libros (isbn);

--
-- Create foreign key
--
ALTER TABLE inspiran
ADD CONSTRAINT fk_inspiran_piratas FOREIGN KEY (nombre_piratas)
REFERENCES piratas (nombre);

--
-- Create table `profesiones`
--
CREATE TABLE profesiones (
  nombre varchar(100) NOT NULL,
  PRIMARY KEY (nombre)
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci,
COMMENT = 'Informaci�n de las profesiones.';

--
-- Create table `ejercian`
--
CREATE TABLE ejercian (
  codigo_ejercian int(11) NOT NULL AUTO_INCREMENT,
  nombre_piratas varchar(100) DEFAULT NULL,
  nombre_profesiones varchar(100) DEFAULT NULL,
  PRIMARY KEY (codigo_ejercian)
)
ENGINE = INNODB,
AUTO_INCREMENT = 2,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci,
COMMENT = 'Informaci�n de las profesiones ejercidas.';

--
-- Create index `uk_piratas_profesiones` on table `ejercian`
--
ALTER TABLE ejercian
ADD UNIQUE INDEX uk_piratas_profesiones (nombre_piratas, nombre_profesiones);

--
-- Create foreign key
--
ALTER TABLE ejercian
ADD CONSTRAINT FK_ejercian_profesiones FOREIGN KEY (nombre_profesiones)
REFERENCES profesiones (nombre) ON DELETE NO ACTION;

--
-- Create foreign key
--
ALTER TABLE ejercian
ADD CONSTRAINT fk_ejercian_piratas FOREIGN KEY (nombre_piratas)
REFERENCES piratas (nombre);

-- 
-- Dumping data for table paises
--
INSERT INTO paises VALUES
('Reino Unido', 'Guillermo III de Inglaterra');

-- 
-- Dumping data for table barcos
--
INSERT INTO barcos VALUES
('Adventure Galley', 'Fragata de remos', 34, 'William Kidd');

-- 
-- Dumping data for table zonas
--
INSERT INTO zonas VALUES
('Oc�ano �ndico', '71�52''35.5'''' E', '6�20''35.5'''' S', 'Reino Unido', NULL);

-- 
-- Dumping data for table libros
--
INSERT INTO libros VALUES
('9788496975880', 'La Isla del Tesoro', '1883', 'Robert Louis Stevenson');

-- 
-- Dumping data for table profesiones
--
INSERT INTO profesiones VALUES
('Marino y militar');

-- 
-- Dumping data for table piratas
--
INSERT INTO piratas VALUES
('William Kidd', 'William "Captain" Kidd', '1655-01-22', '1701-05-23', 'Fue un marino escoc�s recordado por su juicio y ejecuci�n por pirater�a despu�s de regresar de un viaje al oc�ano �ndico. ', 'Reino Unido', 'Adventure Galley');

-- 
-- Dumping data for table roban
--
INSERT INTO roban VALUES
(1, 'William Kidd', 'Oc�ano �ndico');

-- 
-- Dumping data for table inspiran
--
INSERT INTO inspiran VALUES
(1, 'William Kidd', '9788496975880');

-- 
-- Dumping data for table ejercian
--
INSERT INTO ejercian VALUES
(1, 'William Kidd', 'Marino y militar');

-- 
-- Dumping data for table contratan
--
INSERT INTO contratan VALUES
(1, '1688-09-27', 'Reino Unido', 'William Kidd');

-- 
-- Dumping data for table armas
--
INSERT INTO armas VALUES
(1, 'William Kidd', 'Espada');

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;