<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profesiones".
 *
 * @property string $nombre
 *
 * @property Ejercian[] $ejercians
 * @property Piratas[] $nombrePiratas
 */
class Profesiones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profesiones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'match', 'pattern' =>  "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ.,;\s]+$/",'message' => 'Sólo letras.'],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre de la profesión',
        ];
    }

    /**
     * Gets query for [[Ejercians]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEjercians()
    {
        return $this->hasMany(Ejercian::class, ['nombre_profesiones' => 'nombre']);
    }

    /**
     * Gets query for [[NombrePiratas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombrePiratas()
    {
        return $this->hasMany(Piratas::class, ['nombre' => 'nombre_piratas'])->viaTable('ejercian', ['nombre_profesiones' => 'nombre']);
    }
}
