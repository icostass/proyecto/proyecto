<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "zonas".
 *
 * @property string $nombre
 * @property string $longitud
 * @property string $latitud
 * @property string $nombre_paises
 * @property string|null $fecha_dominio
 *
 * @property Paises $nombrePaises
 * @property Piratas[] $nombrePiratas
 * @property Roban[] $robans
 */
class Zonas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zonas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'longitud', 'latitud', 'nombre_paises'], 'required'],
            [['fecha_dominio'], 'date'],
            [[ 'longitud', 'latitud', ],'match', 'pattern' =>  "/^[0-9a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ.,;\s]+$/"],
            [['nombre','nombre_paises'], 'match', 'pattern' =>  "/^[0-9a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ.,;\s]+$/", 'message' => 'Sólo letras.'], 
            [['nombre'], 'unique'],
            [['nombre_paises'], 'exist', 'skipOnError' => true, 'targetClass' => Paises::class, 'targetAttribute' => ['nombre_paises' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre de la zona',
            'longitud' => 'Longitud',
            'latitud' => 'Latitud',
            'nombre_paises' => 'Nombre del país que la dominaba',
            'fecha_dominio' => 'Fecha bajo el dominio',
        ];
    }

    /**
     * Gets query for [[NombrePaises]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombrePaises()
    {
        return $this->hasOne(Paises::class, ['nombre' => 'nombre_paises']);
    }

    /**
     * Gets query for [[NombrePiratas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombrePiratas()
    {
        return $this->hasMany(Piratas::class, ['nombre' => 'nombre_piratas'])->viaTable('roban', ['nombre_zonas' => 'nombre']);
    }

    /**
     * Gets query for [[Robans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRobans()
    {
        return $this->hasMany(Roban::class, ['nombre_zonas' => 'nombre']);
    }
}
