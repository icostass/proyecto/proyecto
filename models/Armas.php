<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "armas".
 *
 * @property int $codigo_armas
 * @property string $nombre_piratas
 * @property string $armas
 *
 * @property Piratas $nombrePiratas
 */
class Armas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'armas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_piratas', 'armas'], 'required'],
            [['nombre_piratas'], 'match', 'pattern' =>  "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ.,;\s]+$/",'message' => 'Sólo letras.'],
            [['armas'], 'match', 'pattern' =>  "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ.,;\s]+$/",'message' => 'Sólo letras.'],
            [['nombre_piratas', 'armas'], 'unique', 'targetAttribute' => ['nombre_piratas', 'armas']],
            [['nombre_piratas'], 'exist', 'skipOnError' => true, 'targetClass' => Piratas::class, 'targetAttribute' => ['nombre_piratas' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_armas' => 'Código Armas',
            'nombre_piratas' => 'Nombre del pirata que usa el arma',
            'armas' => 'Nombre del arma',
        ];
    }

    /**
     * Gets query for [[NombrePiratas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombrePiratas()
    {
        return $this->hasOne(Piratas::class, ['nombre' => 'nombre_piratas']);
    }
}
