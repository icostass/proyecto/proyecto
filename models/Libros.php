<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "libros".
 *
 * @property string $isbn
 * @property string $nombre
 * @property string|null $fecha_publicacion
 * @property string|null $autor
 *
 * @property Inspiran[] $inspirans
 * @property Piratas[] $nombrePiratas
 */
class Libros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'libros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['isbn', 'nombre'], 'required'],
            [['fecha_publicacion'], 'date', 'message' => 'Sólo fechas válidas.'],
            [['isbn'], 'match', 'pattern' => "/^[0-9]+$/", 'message' => 'Sólo números positivos.', 'min' => 13,'max' => 13],
            [['nombre', 'autor'], 'match', 'pattern' =>  "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ.,;\s]+$/",'message' => 'Sólo letras.'],
            [['isbn'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'isbn' => 'Isbn',
            'nombre' => 'Título del libro',
            'fecha_publicacion' => 'Fecha de publicación',
            'autor' => 'Autor',
        ];
    }

    /**
     * Gets query for [[Inspirans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspirans()
    {
        return $this->hasMany(Inspiran::class, ['codigo_isbn' => 'isbn']);
    }

    /**
     * Gets query for [[NombrePiratas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombrePiratas()
    {
        return $this->hasMany(Piratas::class, ['nombre' => 'nombre_piratas'])->viaTable('inspiran', ['codigo_isbn' => 'isbn']);
    }
}
