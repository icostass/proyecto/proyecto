<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "roban".
 *
 * @property int $codigo_roban
 * @property string|null $nombre_piratas
 * @property string|null $nombre_zonas
 *
 * @property Piratas $nombrePiratas
 * @property Zonas $nombreZonas
 */
class Roban extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'roban';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_piratas', 'nombre_zonas'], 'match', 'pattern' =>  "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ.,;\s]+$/",'message' => 'Sólo letras.'],
            [['nombre_piratas', 'nombre_zonas'], 'unique', 'targetAttribute' => ['nombre_piratas', 'nombre_zonas']],
            [['nombre_piratas'], 'exist', 'skipOnError' => true, 'targetClass' => Piratas::class, 'targetAttribute' => ['nombre_piratas' => 'nombre']],
            [['nombre_zonas'], 'exist', 'skipOnError' => true, 'targetClass' => Zonas::class, 'targetAttribute' => ['nombre_zonas' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_roban' => 'Codigo Roban',
            'nombre_piratas' => 'Nombre del pirata',
            'nombre_zonas' => 'Nombre de la zona',
        ];
    }

    /**
     * Gets query for [[NombrePiratas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombrePiratas()
    {
        return $this->hasOne(Piratas::class, ['nombre' => 'nombre_piratas']);
    }

    /**
     * Gets query for [[NombreZonas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreZonas()
    {
        return $this->hasOne(Zonas::class, ['nombre' => 'nombre_zonas']);
    }
}
