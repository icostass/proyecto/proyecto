<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ejercian".
 *
 * @property int $codigo_ejercian
 * @property string|null $nombre_piratas
 * @property string|null $nombre_profesiones
 *
 * @property Piratas $nombrePiratas
 * @property Profesiones $nombreProfesiones
 */
class Ejercian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ejercian';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_piratas', 'nombre_profesiones'], 'match', 'pattern' =>  "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ.,;\s]+$/",'message' => 'Sólo letras.'],
            [['nombre_piratas', 'nombre_profesiones'], 'unique', 'targetAttribute' => ['nombre_piratas', 'nombre_profesiones']],
            [['nombre_piratas'], 'exist', 'skipOnError' => true, 'targetClass' => Piratas::class, 'targetAttribute' => ['nombre_piratas' => 'nombre']],
            [['nombre_profesiones'], 'exist', 'skipOnError' => true, 'targetClass' => Profesiones::class, 'targetAttribute' => ['nombre_profesiones' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_ejercian' => 'Codigo Ejercian',
            'nombre_piratas' => 'Nombre del pirata',
            'nombre_profesiones' => 'Nombre de la profesión',
        ];
    }

    /**
     * Gets query for [[NombrePiratas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombrePiratas()
    {
        return $this->hasOne(Piratas::class, ['nombre' => 'nombre_piratas']);
    }

    /**
     * Gets query for [[NombreProfesiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreProfesiones()
    {
        return $this->hasOne(Profesiones::class, ['nombre' => 'nombre_profesiones']);
    }
}
