<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contratan".
 *
 * @property int $codigo_contratan
 * @property string|null $fecha_contrato
 * @property string $nombre_paises
 * @property string $nombre_piratas
 *
 * @property Paises $nombrePaises
 * @property Piratas $nombrePiratas
 */
class Contratan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contratan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_contrato'], 'date', 'message' => 'Sólo fechas válidas.'],
            [['nombre_paises', 'nombre_piratas'], 'required'],
            [['nombre_paises', 'nombre_piratas'], 'match', 'pattern' =>  "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ.,;\s]+$/",'message' => 'Sólo letras.'],
            [['nombre_paises', 'nombre_piratas'], 'unique', 'targetAttribute' => ['nombre_paises', 'nombre_piratas']],
            [['nombre_paises'], 'exist', 'skipOnError' => true, 'targetClass' => Paises::class, 'targetAttribute' => ['nombre_paises' => 'nombre']],
            [['nombre_piratas'], 'exist', 'skipOnError' => true, 'targetClass' => Piratas::class, 'targetAttribute' => ['nombre_piratas' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_contratan' => 'Código contratación',
            'fecha_contrato' => 'Fecha del contrato',
            'nombre_paises' => 'Nombre del país',
            'nombre_piratas' => 'Nombre del pirata',
        ];
    }

    /**
     * Gets query for [[NombrePaises]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombrePaises()
    {
        return $this->hasOne(Paises::class, ['nombre' => 'nombre_paises']);
    }

    /**
     * Gets query for [[NombrePiratas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombrePiratas()
    {
        return $this->hasOne(Piratas::class, ['nombre' => 'nombre_piratas']);
    }
}
