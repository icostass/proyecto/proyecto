<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "barcos".
 *
 * @property string $nombre
 * @property string $tipo
 * @property int|null $numero_canones
 * @property string|null $capitan
 *
 * @property Piratas[] $piratas
 */
class Barcos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'barcos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'tipo'], 'required'],
            [['numero_canones'],'match', 'pattern' => "/^[0-9]+$/", 'message' => 'Sólo números positivos.'],
            [['nombre'], 'match', 'pattern' =>  "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ.,;\s]+$/",'message' => 'Sólo letras.'],
            [['tipo', 'capitan'], 'match', 'pattern' =>  "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ.,;\s]+$/",'message' => 'Sólo letras.'],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre de barco',
            'tipo' => 'Tipo',
            'numero_canones' => 'Número de cañones',
            'capitan' => 'Capitán del barco',
        ];
    }

    /**
     * Gets query for [[Piratas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPiratas()
    {
        return $this->hasMany(Piratas::class, ['nombre_barcos' => 'nombre']);
    }
}
