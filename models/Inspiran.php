<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inspiran".
 *
 * @property int $codigo_inspiran
 * @property string|null $nombre_piratas
 * @property string|null $codigo_isbn
 *
 * @property Libros $codigoIsbn
 * @property Piratas $nombrePiratas
 */
class Inspiran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inspiran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_piratas'], 'match', 'pattern' =>  "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ.,;\s]+$/",'message' => 'Sólo letras.'],
            [['codigo_isbn'],'match', 'pattern' => "/^[0-9]+$/", 'message' => 'Sólo números positivos.','min' => 13 ,'max' => 13],
            [['nombre_piratas', 'codigo_isbn'], 'unique', 'targetAttribute' => ['nombre_piratas', 'codigo_isbn']],
            [['codigo_isbn'], 'exist', 'skipOnError' => true, 'targetClass' => Libros::class, 'targetAttribute' => ['codigo_isbn' => 'isbn']],
            [['nombre_piratas'], 'exist', 'skipOnError' => true, 'targetClass' => Piratas::class, 'targetAttribute' => ['nombre_piratas' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_inspiran' => 'Codigo Inspiran',
            'nombre_piratas' => 'Nombre del pirata',
            'codigo_isbn' => 'Código Isbn',
        ];
    }

    /**
     * Gets query for [[CodigoIsbn]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoIsbn()
    {
        return $this->hasOne(Libros::class, ['isbn' => 'codigo_isbn']);
    }

    /**
     * Gets query for [[NombrePiratas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombrePiratas()
    {
        return $this->hasOne(Piratas::class, ['nombre' => 'nombre_piratas']);
    }
}
