<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "piratas".
 *
 * @property string $nombre
 * @property string|null $mote
 * @property string|null $f_nacimiento
 * @property string|null $f_muerte
 * @property string|null $biografia
 * @property string|null $nombre_paises
 * @property string|null $nombre_barcos
 *
 * @property Armas[] $armas
 * @property Libros[] $codigoIsbns
 * @property Contratan[] $contratans
 * @property Ejercian[] $ejercians
 * @property Inspiran[] $inspirans
 * @property Barcos $nombreBarcos
 * @property Paises $nombrePaises
 * @property Paises[] $nombrePaises0
 * @property Profesiones[] $nombreProfesiones
 * @property Zonas[] $nombreZonas
 * @property Roban[] $robans
 */
class Piratas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piratas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required', 'message' => 'Este campo es obligatorio.' ],
            [['f_nacimiento', 'f_muerte'], 'date', 'message' => 'Sólo fechas válidas.'],
            [['biografia'], 'default', 'value' => null],
            [['nombre', 'nombre_paises', 'nombre_barcos'], 'match', 'pattern' =>  "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ.,;\s]+$/",'message' => 'Sólo letras.'],
            [['mote'],'match', 'pattern' =>  "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ.,;\s]+$/" ,'message' => 'Sólo letras.'],
            [['nombre'], 'unique'],
            [['nombre_barcos'], 'exist', 'skipOnError' => true, 'targetClass' => Barcos::class, 'targetAttribute' => ['nombre_barcos' => 'nombre']],
            [['nombre_paises'], 'exist', 'skipOnError' => true, 'targetClass' => Paises::class, 'targetAttribute' => ['nombre_paises' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'mote' => 'Mote',
            'f_nacimiento' => 'Fecha de nacimiento',
            'f_muerte' => 'Fecha de muerte',
            'biografia' => 'Biografia',
            'nombre_paises' => 'País',
            'nombre_barcos' => 'Barco',
        ];
    }

    /**
     * Gets query for [[Armas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArmas()
    {
        return $this->hasMany(Armas::class, ['nombre_piratas' => 'nombre']);
    }

    /**
     * Gets query for [[CodigoIsbns]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoIsbns()
    {
        return $this->hasMany(Libros::class, ['isbn' => 'codigo_isbn'])->viaTable('inspiran', ['nombre_piratas' => 'nombre']);
    }

    /**
     * Gets query for [[Contratans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratans()
    {
        return $this->hasMany(Contratan::class, ['nombre_piratas' => 'nombre']);
    }

    /**
     * Gets query for [[Ejercians]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEjercians()
    {
        return $this->hasMany(Ejercian::class, ['nombre_piratas' => 'nombre']);
    }

    /**
     * Gets query for [[Inspirans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspirans()
    {
        return $this->hasMany(Inspiran::class, ['nombre_piratas' => 'nombre']);
    }

    /**
     * Gets query for [[NombreBarcos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreBarcos()
    {
        return $this->hasOne(Barcos::class, ['nombre' => 'nombre_barcos']);
    }

    /**
     * Gets query for [[NombrePaises]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombrePaises()
    {
        return $this->hasOne(Paises::class, ['nombre' => 'nombre_paises']);
    }

    /**
     * Gets query for [[NombrePaises0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombrePaises0()
    {
        return $this->hasMany(Paises::class, ['nombre' => 'nombre_paises'])->viaTable('contratan', ['nombre_piratas' => 'nombre']);
    }

    /**
     * Gets query for [[NombreProfesiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreProfesiones()
    {
        return $this->hasMany(Profesiones::class, ['nombre' => 'nombre_profesiones'])->viaTable('ejercian', ['nombre_piratas' => 'nombre']);
    }

    /**
     * Gets query for [[NombreZonas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreZonas()
    {
        return $this->hasMany(Zonas::class, ['nombre' => 'nombre_zonas'])->viaTable('roban', ['nombre_piratas' => 'nombre']);
    }

    /**
     * Gets query for [[Robans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRobans()
    {
        return $this->hasMany(Roban::class, ['nombre_piratas' => 'nombre']);
    }
}
