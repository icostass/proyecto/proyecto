<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paises".
 *
 * @property string $nombre
 * @property string|null $gobernante
 *
 * @property Contratan[] $contratans
 * @property Piratas[] $nombrePiratas
 * @property Piratas[] $piratas
 * @property Zonas[] $zonas
 */
class Paises extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paises';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre', 'gobernante'], 'match', 'pattern' =>  "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ.,;\s]+$/",'message' => 'Números no válidos.'],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre del país',
            'gobernante' => 'Gobernante',
        ];
    }

    /**
     * Gets query for [[Contratans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratans()
    {
        return $this->hasMany(Contratan::class, ['nombre_paises' => 'nombre']);
    }

    /**
     * Gets query for [[NombrePiratas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombrePiratas()
    {
        return $this->hasMany(Piratas::class, ['nombre' => 'nombre_piratas'])->viaTable('contratan', ['nombre_paises' => 'nombre']);
    }

    /**
     * Gets query for [[Piratas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPiratas()
    {
        return $this->hasMany(Piratas::class, ['nombre_paises' => 'nombre']);
    }

    /**
     * Gets query for [[Zonas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZonas()
    {
        return $this->hasMany(Zonas::class, ['nombre_paises' => 'nombre']);
    }
}
